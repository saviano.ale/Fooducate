//
//  horizontal scrool.swift
//  Prova
//
//  Created by Roberto Farella on 19/11/21.
//

import Foundation
import SwiftUI


struct CircleView: View {
    @State var label: String
    
    var body: some View {
        ZStack {
            Rectangle()
                .fill(Color.gray)
                .frame(width: 130, height: 50)
            Text(label)
        }
    }
}
