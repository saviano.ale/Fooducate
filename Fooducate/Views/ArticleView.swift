//
//  ArticleView.swift
//  Fooducate
//
//  Created by Giacomo Marco La Montagna on 18/11/21.
//

import SwiftUI

struct ArticleView: View {
    
    @State public var article: Article
    
    var body: some View {
        VStack(alignment: .leading) {
            Image(uiImage: article.image)
                .resizable()
                .scaledToFill() // add if you need
                .frame(height: 150.0) // as per your requirement
                .clipped()
                .padding(.top, 10)
            
            
            VStack(alignment: .leading) {
                
                TagList(tags: article.tags)
                
                Text(article.title)
                    .padding(.bottom, 0.5)
                    .textCase(nil)
                    .font(.title3.bold())
                Text(article.paragraph)
//                    .frame(width: 370.0)
                    .padding(.bottom, 35)
                    .lineLimit(article.isExpanded ? nil : 4)
                    .foregroundColor(Color(UIColor.darkGray))
                    .overlay(
                        GeometryReader { proxy in
                            Button(action: {
                                article.isExpanded.toggle()
                            }) {
                                Text(article.isExpanded ? "Read less" : "Read more")
                                    .font(.body)
                                    .foregroundColor(.blue)
                                    .padding(.bottom, 5)
                            }
                            .frame(width: proxy.size.width, height: proxy.size.height, alignment: .bottomLeading)
                        }
                    )
                
            }.padding(.bottom, 10).padding(.top, 5)
        }
    }
}

//struct ContentView_Previews: PreviewProvider {
//    static var previews: some View {
//        RecipesList()
//    }
//}


