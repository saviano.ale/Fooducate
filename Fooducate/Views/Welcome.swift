//
//  Welcome.swift
//  Fooducate
//
//  Created by Alessia Saviano on 18/11/21.
//

import SwiftUI

struct Welcome: View {
    @EnvironmentObject var childrenStore: ChildrenStore
    @AppStorage("child1Name") var child1Name: String?
    @AppStorage("child1Age") var child1Age: Int?
    @AppStorage("child1Picked") var child1Picked: Bool?

    @State var sliderValue: Double = 3.0
    @State private var name: String = ""
    
    
    @AppStorage("isOnboarding") var isOnboarding: Bool?
    
    var body: some View {
        
        NavigationView {
            
            ZStack {
                Color.init(red: 242/255, green: 242/255, blue: 247/255)
                    .ignoresSafeArea()
                ScrollView{
                    VStack {
                        Text("Enter your child’s name and age for an accurate experience! No data will be sent to us.")
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .padding(.horizontal, 20)
                        
                        Section {
                            Image("Kids")
                                .resizable()
                                .scaledToFit()
                                .frame(maxWidth: 300)
                                .padding(.bottom, 30).padding(.top, 10)
                        }
                        
                        Section(header: Text("CHILD'S NAME")
                                    .font(.footnote)
                                    .foregroundColor(Color(UIColor.gray))
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .padding(.horizontal, 20)) {
                            TextField("Name", text: $name)
                                .textFieldStyle(RoundedBorderTextFieldStyle())
//                                .frame(maxWidth: .infinity)
                                .padding(.horizontal, 20)
                        }
                        
                        Section(header: Text("CHILD'S AGE")
                                    .padding(.top, 30)
                                    .font(.footnote)
                                    .foregroundColor(Color(UIColor.gray))
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .padding(.horizontal, 20)) {
                            
                            Slider(value: $sliderValue, in: 3...13, step: 1)
                                .padding(.horizontal, 20)
                            
                            HStack(alignment: .top) {
                                Text("3")
                                    .font(.subheadline)
                                    .foregroundColor(Color.gray)
                                Spacer()
                                Text(" \(sliderValue, specifier: "%.0f")")
                                    .accentColor(Color.blue)
                                Spacer()
                                Text("13")
                                    .font(.subheadline)
                                    .foregroundColor(Color.gray)
                            }.padding(.horizontal, 20)
                        }
                        
                        Section {
                            Button(action: {
                                if (name != "") {
                                    isOnboarding = false
                                   
                                    child1Name = name
                                    child1Age = Int(sliderValue)
                                    child1Picked = true
                                    
                                    childrenStore.children[0].name = child1Name!
                                    childrenStore.children[0].age = child1Age!
                                    childrenStore.children[0].picked = child1Picked!
                                    
                                    
                                }}, label: {
                                Text("Start")
                                    .fontWeight(.bold)
                                    .foregroundColor(Color.white)
                                    .multilineTextAlignment(.center)
                                    .padding(15)
                                    .frame(maxWidth: .infinity)
                                    .background(Color.blue)
                                    .cornerRadius(10)
                            })
                        }.padding(.horizontal, 20).padding(.vertical, 30)
                    }.navigationTitle("Welcome")
                }
            }
        }
    }
    
    struct Welcome_Previews: PreviewProvider {
        static var previews: some View {
            Welcome()
        }
    }
}
