import SwiftUI


struct Bottone: View {
    
    var name: String = " "
    var index: Int = 0
    @State var buttonActive: Bool = false
    @State var background: Color = Color.gray
    @State var opacity: Double = 0.1
    @State var text: Color = Color(UIColor.darkGray)
    
    
    @inlinable public init(index: Int, name: String
    ){
        self.index = index
        self.name = name
    }
    
    var body: some View {
        
        VStack(alignment: .leading) {
            
            Button(action: {
                buttonActive.toggle()
                if buttonActive{
                    background = Color.blue
                    opacity = 1.0
                    text = Color.white
                    
                } else {
                    background = Color(UIColor.darkGray)
                    opacity = 0.1
                    text = Color(UIColor.darkGray)
                }
            })  {
                
                Text(name)
                    .font(.system(size: 15.0))
                    .foregroundColor(text)
                    .multilineTextAlignment(.center)
                    .padding(12.0)
                    .frame(maxWidth: .infinity)
            }
            
        }.background(
            background
                .opacity(opacity)
        ).clipShape(RoundedRectangle(cornerRadius: 8))
        
    }

}
