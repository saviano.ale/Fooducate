//
//  ScrollTagList.swift
//  Fooducate
//
//  Created by Giacomo Marco La Montagna on 24/11/21.
//

import SwiftUI

struct ScrollTagList: View {
    
    var suggestion = ["Fruits","Vegetables","Grains","Meat/Fish","Dairy","Healthy life"]
    
    var body: some View {
        ScrollView(.horizontal) {
            HStack(spacing: 10) {
                ForEach(0..<6) { Index in
                    Bottone(index: Index, name: suggestion[Index])
                }
            }.padding(.horizontal).padding(.bottom, 5)
        }
        Divider()
    }
}

struct ScrollTagList_Previews: PreviewProvider {
    static var previews: some View {
        ScrollTagList()
    }
}
