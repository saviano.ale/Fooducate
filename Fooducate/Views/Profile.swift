//
//  Profile.swift
//  Fooducate
//
//  Created by Alessia Saviano on 17/11/21.
//

import Foundation
import SwiftUI


struct Profile: View{
    
    @Binding var isPresented: Bool
    @Binding var food: [Food]
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var childrenStore: ChildrenStore
    @State var isDisclosed = false
    @State var count: Int = 0
    @State var check: [Bool] = []
    
    @AppStorage("child1Name") var child1Name: String?
    @AppStorage("child1Age") var child1Age: Int?
    @AppStorage("child1Picked") var child1Picked: Bool?
    
    
    var body: some View {
        
        NavigationView {
            
            ZStack {
                Color.init(red: 242/255, green: 242/255, blue: 247/255)
                    .ignoresSafeArea()
                
                VStack {
                    if (childrenStore.children.count > 1) {
                        List {
                            Section(header: Text("Your children"), footer:Text("Here you select which child you want to view goals.")) {
                                ForEach (childrenStore.children) { child in
                                    HStack {
                                        Text("\(child.name)")
                                        Spacer()
                                        Button(action: {
                                            if(!child.picked){
                                                child.picked.toggle()
//                                                count = Int(child.id)
                                                
                                            }
                                            if child.picked {
                                                for i in childrenStore.children{
                                                    if(i.name != child.name && i.age != child.age){
                                                        i.picked = false
                                                    }
                                                    check.append(child.picked)
                                                }
                                            }
                                        }) {
                                            if child.picked {
                                                Image(systemName: "checkmark")
                                                
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        .frame(height: (childrenStore.children.count > 2 ? 185 : 135))
                        .padding(.top, 41)
                    }
                    
                    List(childrenStore.children) { child in
                        Section(header:
                                    HStack { Text("CHILD")
                            Spacer()
                            Button("Edit") {
                                print("Button tapped!")
                            }
                        }, content: {
                            HStack {
                                Text("Name")
                                    .foregroundColor(.black)
                                Spacer()
                                Text("\(child.name)")
                                    .foregroundColor(.gray)
                            }
                            HStack {
                                Text("Age")
                                Spacer()
                                Text("\(child.age)")
                                    .foregroundColor(.gray)
                            }
                        }
                        )}.padding(.top, 35)
                    
                    
                    Button(action: {
                        print("Add Child Click")
                    }, label: {
                        NavigationLink(destination: AddChild()) {
                            Text("Add Child")
                                .frame(maxWidth: .infinity, maxHeight: 48)
                        }
                    }).background(.white).clipShape(RoundedRectangle(cornerRadius: 13))
                        .padding(.vertical, 5).padding(.horizontal, 20)
                    
                }
            }
            .navigationBarTitle("Profile", displayMode: .inline)
            .toolbar {
                ToolbarItem(placement: .primaryAction) {
                    Button(action: {
                        presentationMode.wrappedValue.dismiss()
                    }) {
                        Text("Done").fontWeight(.bold)
                    }
                }
            }
        }
    }
}
