//
//  Suggestions.swift
//  Fooducate
//
//  Created by Alessia Saviano on 14/11/21.
//

import SwiftUI

struct SuggestionsView: View {
    
    @State var selected = 2
    @State private var isExpanded: Bool = false
    
    //    var articles: [Article] = [Article(title: "Pizza", paragraph: "To prepare the American carrot cake recipe, first, wash and peel the carrots. Grate them and put them in a bowl. Add the coarsely chopped walnuts to", image: UIImage(named: "Pizza")!),
    //                               Article(title: "Burger", paragraph: "To prepare the American carrot cake recipe, first, wash and peel the carrots. Grate them and put them in a bowl. Add the coarsely chopped walnuts to", image: UIImage(named: "Burger")!),
    //                               Article(title: "Pasta", paragraph: "To prepare the American carrot cake recipe, first, wash and peel the carrots. Grate them and put them in a bowl. Add the coarsely chopped walnuts to", image: UIImage(named: "Pasta")!)]
    //
    //    Article(title: "Pizza",paragraph: "To prepare the American carrot cake recipe, first, wash and peel the carrots. Grate them and put them in a bowl. Add the coarsely chopped walnuts to", image: #imageLiteral(resourceName: "1") ), Article(title: "Pasta", paragraph: "La pasta è sacra" ,image: #imageLiteral(resourceName: "Image-1")), Article(title: "Panino",
    //                  paragraph: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
    //                  image: #imageLiteral(resourceName: "Image"))
    
    var body: some View {
        
        NavigationView {
            ZStack {
                Color.init(red: 242/255, green: 242/255, blue: 247/255)
                    .ignoresSafeArea()
                
                VStack {
                    
                    Picker(selection: $selected, label: Text("Picker"), content: {
                        Text("Recipes")
                          .tag(2)
                            
                        Text("Tips")
                            .tag(1)
                        
                    })
                    
                        .pickerStyle(SegmentedPickerStyle())
                        .padding()
                    
                    
                    if selected == 1 {
                        ScrollTagList()
                        TipsList()
                        
                    } else {
                        ScrollTagList()
                        RecipesList()
                        
                        
                        //                        VStack(alignment: .leading) {
                        //                            List(articles,
                        //                                 id: \.title,
                        //                                 rowContent: { article in
                        //                                Image(uiImage: article.image)
                        //                                    .resizable()
                        //                                    .aspectRatio(contentMode: .fill)
                        //                                    .frame(width: 350, height: 130)
                        //                                    .listRowBackground(Color.clear)
                        //                                    .listRowInsets(.init(top: 0, leading: 0, bottom: 0, trailing: 0))
                        //                                    .listRowSeparator(.hidden)
                        //                                    .padding(.top, 50) .padding(.bottom, 20)
                        //
                        //                                VStack(alignment: .leading) {
                        //                                    Text(article.title)
                        //                                        .textCase(nil)
                        //                                        .font(.title3.bold()).padding(.top, 10)
                        //                                    Text(article.paragraph).padding(.bottom, 30)
                        //                                        .lineLimit(isExpanded ? nil : 3)
                        //                                        .overlay(
                        //                                        GeometryReader { proxy in
                        //                                            Button(action: {
                        //                                                isExpanded.toggle()
                        //                                            }) {
                        //                                                Text(isExpanded ? "Read less" : "Read more")
                        //                                                    .font(.body)
                        //                                            }
                        //                                            .frame(width: proxy.size.width, height: proxy.size.height, alignment: .bottomLeading)
                        //                                        }
                        //                                    )
                        //                              }
                        //
                        //                                //                                VStack(alignment: .leading) {
                        //                                //                                    Text(article.title)
                        //                                //                                        .textCase(nil)
                        //                                //                                        .font(.title3.bold()).padding(.top)
                        //                                //                                    Text(article.paragraph)
                        //                                //                                        .font(.body).padding(.top, -15).padding(.bottom)
                        //                                //                                }
                        //                                .listRowBackground(Color.clear)
                        //                                .listRowInsets(.init(top: 0, leading: 0, bottom: 0, trailing: 0))
                        //                            }).listStyle(InsetGroupedListStyle())
                        //                        }
                        
                    }
                }
            }
            .navigationTitle("Suggestions")
        }
    }
}

//struct ContentView_Previews: PreviewProvider {
//    static var previews: some View {
//        ContentView()
//    }
//}
