//
//  RecipesList.swift
//  Fooducate
//
//  Created by Giacomo Marco La Montagna on 19/11/21.
//

import SwiftUI

struct RecipesList: View {
    
    @StateObject var recipesStore = RecipesStore()
    
    var body: some View {
        
        List {
            ForEach(recipesStore.recipes) {
                recipe in ArticleView(article: recipe).listRowSeparator(.hidden)
            }
            .listRowBackground(Color.clear)
            .listRowInsets(.init(top: 0, leading: 0, bottom: 0, trailing: 0))
        }.listStyle(InsetGroupedListStyle())
            .onAppear(perform: {
                    UITableView.appearance().contentInset.top = -35
                })
        
    }
}


//struct RecipesList_Previews: PreviewProvider {
//    static var previews: some View {
//        RecipesList()
//    }
//}
