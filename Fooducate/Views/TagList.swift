//
//  TagList.swift
//  Fooducate
//
//  Created by Giacomo Marco La Montagna on 24/11/21.
//

import SwiftUI


struct TagList: View {
    
    var tags: [String]
    
    
    var body: some View {
        HStack(spacing: 10) {
            ForEach(tags, id: \.self) { tag in
                Text(tag)
                    .foregroundColor(Color(UIColor.darkGray))
                        .multilineTextAlignment(.center)
                        .font(.system(size: 15))
                        .padding(10)
                        .background(
                            Color.gray
                                .opacity(0.1)
                        ).clipShape(RoundedRectangle(cornerRadius: 8))
            }
        }.padding(.trailing)
    
    }
}

//struct TagList_Previews: PreviewProvider {
//    static var previews: some View {
//        TagList()
//    }
//}
