//
//  Goals.swift
//  Fooducate
//
//  Created by Alessia Saviano on 14/11/21.
//

import Foundation
import SwiftUI

struct Food : Hashable {
    var id = UUID()
    var name: String
    var check: Bool
}

struct GoalsView: View {
    @EnvironmentObject var childrenStore: ChildrenStore
    
    
    
    @State var food =
    [Food(name: "🍎 Fruits", check: false), Food(name: "🥕 Vegetables", check: false), Food(name: "🍞 Grains", check: false), Food(name: "🍗 Meat or fish", check: false), Food(name: "🧀  Dairy", check: false)]
    @State var progressValue: Float = 0.0
    @State var isPresented: Bool = false

    
    
    var body: some View {
        
        NavigationView {
            
            ZStack {
                Color.init(red: 242/255, green: 242/255, blue: 247/255)
                    .ignoresSafeArea()
                
                VStack {
                    Text("Here you can gradually mark what you feed your child \(childrenStore.children[0].name).")
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.horizontal, 20)
                    
                    ProgressBar(progress: self.$progressValue)
                        .frame(width: 150.0, height: 150.0)
                        .padding(.vertical, 35.0)
                    
                    List {
                        ForEach(food.indices) {
                            i in HStack {
                                
                                Text("\(self.food[i].name) \(ConversionTable(age: childrenStore.children[0].age).amounts[i])g")
                                Spacer()
                                Button(action: {
                                    food[i].check.toggle()
                                    if (food[i].check) {
                                        progressValue += 0.2
                                    }
                                    else {
                                        progressValue -= 0.2
                                    }
                                }) {
                                    if food[i].check == false {
                                        Image(systemName: "circle")
                                            .foregroundColor(Color.gray)
                                    }
                                    else {
                                        Image(systemName: "checkmark.circle.fill")
                                    }
                                }
                            }
                        }.padding(.vertical, 10)
                    }
                    .padding(.top, 15)
                    .onAppear(perform: {
                        UITableView.appearance().contentInset.top = -35
                    })
                }
                .listRowBackground(Color.red)
                .listStyle(InsetGroupedListStyle())
            }
            .navigationTitle("Goals")
            .toolbar {
                ToolbarItem(placement: .primaryAction) {
                    Button(action: {
                        self.isPresented.toggle()
                    }) {
                        Image(systemName: "person.crop.circle")
                    }
                }
            }
        }
        //    MODALVIEW
        .sheet(isPresented: $isPresented) {
            Profile(isPresented: $isPresented, food: $food)
        }
    }
    
}

//struct GoalsView_Previews: PreviewProvider {
//    static var previews: some View {
//        GoalsView()
//    }
//}
