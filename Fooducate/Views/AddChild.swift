//
//  AddChild.swift
//  Fooducate
//
//  Created by Alessia Saviano on 17/11/21.
//

import Foundation
import SwiftUI

struct AddChild: View{
    
    @EnvironmentObject var childrenStore: ChildrenStore
    @State var isPresented: Bool = false
    @Environment(\.presentationMode) var presentationMode
    @State private var name: String = ""
    @State var sliderValue: Double = 3
    var body: some View {
        
        VStack {
            ZStack {
                Color.init(red: 242/255, green: 242/255, blue: 247/255)
                    .ignoresSafeArea()
                
                VStack {
                    Section(header: Text("CHILD'S NAME")
                                .padding(.top, 35)
                                .font(.footnote)
                                .foregroundColor(Color(UIColor.gray))
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .padding(.horizontal, 20)) {
                        TextField("Name", text: $name)
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                            .padding(.horizontal, 20)
                    }
                    
                    
//                    Section(header:
//                                HStack { Text("CHILD'S AGE")
//                            .padding(.top, 30)
//                            .font(.footnote)
//                            .foregroundColor(Color.gray)
//                            .frame(maxWidth: .infinity, alignment: .leading)
//                        Spacer()
//                        Text(" \(sliderValue, specifier: "%.0f")")
//                            .padding(.top, 30)
//                    }.padding(.horizontal, 20)) {
//
//                        Slider(value: $sliderValue, in: 3...13, step: 1) {
//                        } minimumValueLabel: {
//                            Text("3").font(.callout).foregroundColor(Color.gray)
//                        } maximumValueLabel: {
//                            Text("13").font(.callout).foregroundColor(Color.gray)
//                        }
//                        .padding(.horizontal, 20)
                        
                        
                    Section(header: Text("CHILD'S AGE")
                                .padding(.top, 30)
                                .font(.footnote)
                                .foregroundColor(Color(UIColor.gray))
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .padding(.horizontal, 20)) {

                        Slider(value: $sliderValue, in: 3...13, step: 1)
                            .padding(.horizontal, 20)

                        HStack(alignment: .top) {
                            Text("3")
                                .font(.subheadline)
                                .foregroundColor(Color.gray)
                            Spacer()
                            Text(" \(sliderValue, specifier: "%.0f")")
                                .accentColor(Color.blue)
                            Spacer()
                            Text("13")
                                .font(.subheadline)
                                .foregroundColor(Color.gray)
                        }
                        .padding(.horizontal, 20)

                        Spacer()
                    }
                    
                }
            }
        }
        .navigationBarTitle("Add Child", displayMode: .inline)
        .toolbar {
            ToolbarItem(placement: .primaryAction) {
                Button(action: {
                    if (name != "") {
                    let newChild: Child = Child(name: name, age: Int(sliderValue))
                    childrenStore.children.append(newChild)
                    presentationMode.wrappedValue.dismiss()
                    }
                }) {
                    Text("Save").fontWeight(.bold)
                }
            }
        }
    }
}



//struct AddChild_Previews: PreviewProvider {
//    static var previews: some View {
//        AddChild()
//    }
//}
