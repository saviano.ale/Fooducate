//
//  ContentView.swift
//  Fooducate
//
//  Created by Alessia Saviano on 14/11/21.
//

import SwiftUI

struct ContentView: View {
    @EnvironmentObject var childrenStore: ChildrenStore

    
    var body: some View {
        TabView {
            GoalsView().environmentObject(childrenStore)
             .tabItem {
                Image(systemName: "star.fill")
                Text("Goals")
              }
            
            SuggestionsView()
             .tabItem {
                Image(systemName: "lightbulb.fill")
                Text("Suggestions")
              }
            
        }
        .onAppear {
            if #available(iOS 15.0, *) {
                let appearance = UITabBarAppearance()
                UITabBar.appearance().scrollEdgeAppearance = appearance
            }
        }
    }
    
}

//struct ContentView_Previews: PreviewProvider {
//    static var previews: some View {
//        ContentView()
//    }
//}
