//
//  TipsList.swift
//  Fooducate
//
//  Created by Giacomo Marco La Montagna on 18/11/21.
//

import SwiftUI

struct TipsList: View {
    @StateObject var tipsStore = TipsStore()
    var body: some View {
        
        List{
            ForEach(tipsStore.tips) {
                tip in ArticleView(article: tip).listRowSeparator(.hidden)
            }
            .listRowBackground(Color.clear)
            .listRowInsets(.init(top: 0, leading: 0, bottom: 0, trailing: 0))
        }.listStyle(InsetGroupedListStyle())
    }
    
}

struct TipsList_Previews: PreviewProvider {
    static var previews: some View {
        TipsList()
    }
}
