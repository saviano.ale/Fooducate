//
//  ConversionTable.swift
//  Fooducate
//
//  Created by Giacomo Marco La Montagna on 22/11/21.
//

import Foundation



class ConversionTable{
    
    var age: Int
    
    init(age: Int){
        self.age = age
    }
    
    var amounts: [Int] {
        switch self.age {
        case 3 :
            return [150, 200, 190, 80, 180]
        case 4 :
            return [180, 340, 190, 120, 180]
        case 5 :
            return [180, 340, 190, 120, 180]
        case 6 :
            return [180, 340, 190, 120, 180]
        case 7 :
            return [180, 340, 190, 120, 180]
        case 8 :
            return [180, 340, 190, 120, 180]
        case 9 :
            return [300, 380, 190, 200, 300]
        case 10 :
            return [300, 380, 190, 200, 300]
        case 11 :
            return [300, 380, 190, 200, 300]
        case 12 :
            return [300, 380, 235, 200, 420]
        case 13 :
            return [300, 380, 235, 200, 420]
            
            
            
            
        default :
            return []
        }
    }
    
    
    
    
    
    
    
}
