//
//  TipsStore.swift
//  Fooducate
//
//  Created by Giacomo Marco La Montagna on 18/11/21.
//

import Foundation
import SwiftUI

class TipsStore: ObservableObject{
    @Published var tips: [Article] = []
    
    var tip1 = Article(title: "Be a good role model", paragraph: "If you're constantly on a diet or have erratic eating habits, your children will grow up thinking that this sort of behavior is normal. Be honest with yourself about the food messages you're sending. Trust your body to tell you when you're hungry and when you're full, and your kids will learn to do the same.", image: UIImage(named: "role-model")!, tags: ["Healthy life"])
    var tip2 = Article(title: "Get creative with meals", paragraph: "The more creative the meal is, the greater the variety of foods kids eat. Make smiley-face pancakes and give food silly names, like broccoli florets are baby trees or dinosaur food. Anything mini is always a hit too. Use cookie cutters to turn toast into hearts and stars, which the children love.", image: UIImage(named: "creative-food")!, tags: ["Healthy life"])
    var tip3 = Article(title: "Schedule meals and snacks", paragraph: "Children need to eat every three to four hours: three meals, two snacks, and lots of fluids. If you plan for these, your child's diet will be much more balanced and they'll be less cranky. If planning a weekly menu is too daunting, start with two or three days at a time. A good dinner doesn't have to be fancy, but it should be balanced: whole-grain bread, rice, or pasta; a fruit or a vegetable; and a protein source like lean meat, cheese, or beans.", image: UIImage(named:"meal-planning")!, tags: ["Healthy life"])
    var tip4 = Article(title: "Get your kids cooking", paragraph: "If your children become involved in choosing or preparing meals, they will be more interested in eating what they have created. Take them to the store, and let them choose produce for you. If they're old enough, allow them to cut up vegetables and mix them into a salad. For example, if they refuse to eat fresh fruit, try to make banana or apple muffins together-and they always eat them once they're done.", image: UIImage(named: "cooking")!, tags: ["Vegetables","Grains","Healthy life"])
    init(){
        self.tips = [tip1, tip2, tip3, tip4]
    }
}
