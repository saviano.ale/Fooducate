//
//  Child.swift
//  Fooducate
//
//  Created by Giacomo Marco La Montagna on 19/11/21.
//

import Foundation
import SwiftUI

class Child: Identifiable{
    
    let id = UUID()
    var name: String
    var age: Int
    var picked: Bool = false
    
    init(name: String, age: Int){
        self.name = name
        self.age = age
    }
}
