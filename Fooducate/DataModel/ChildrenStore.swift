//
//  ChildrenStore.swift
//  Fooducate
//
//  Created by Giacomo Marco La Montagna on 19/11/21.
//

import Foundation

class ChildrenStore: ObservableObject {
//
//    internal init(child: Child) {
//        self.children = [child]
//    }
    
    @Published var children: [Child] = [Child(name: "defaultChild", age: 3)]

//    Salvataggio Dati
//    @Published var children: [Child] = UserDefaults.standard.array(forKey: "childrenList") as? [Child] ?? [Child]()
//        func addChildData(_ data: Child) {
//            children.append(data)
//            UserDefaults.standard.set(children, forKey: "childrenList")
//        }
}
