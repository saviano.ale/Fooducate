//
//  Cibo.swift
//  Fooducate
//
//  Created by Alessia Saviano on 15/11/21.
//

import Foundation
import SwiftUI

struct Article: Identifiable {
    
    let id = UUID()
    var title: String
    var paragraph: String
    var image: UIImage
    var isExpanded: Bool = false
    var tags: [String]
}


