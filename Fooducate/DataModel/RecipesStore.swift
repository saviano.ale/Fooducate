//
//  RecipesStore.swift
//  Fooducate
//
//  Created by Giacomo Marco La Montagna on 19/11/21.
//

import Foundation
import SwiftUI

class RecipesStore: ObservableObject{
    @Published var recipes: [Article] = []
    
    var recipe1 = Article(title: "Carrot quinoa muffins", paragraph: "In a large bowl, combine the flour, baking powder, baking soda, salt, cinnamon, coconut, and raisins. Stir until well combined. In a separate bowl, whisk the egg, maple syrup and yoghurt together until well combined. Stir through the grated carrots. Gently combine the two mixtures together. Dollop batter into the muffin pan and bake for around 20 - 30 minutes, or until the tops spring back when gently pressed.", image: UIImage(named: "Carrot")!, tags: ["Vegetables","Grains"])
    var recipe2 = Article(title: "Creamy broccoli cheddar soup", paragraph: "Melt butter in a large dutch oven or pot over medium-high heat. Add the onion and cook 3-4 minutes or until softened and light gold. Add the garlic and saute for another minute. Add flour and whisk for 1-2 minutes or until the flour begins to turn golden in color. Pour in the chicken stock, broccoli florets, carrots, and seasoning. Bring to a boil then reduce heat to medium-low and simmer for 15 minutes or until the broccoli and carrots are cooked through.Stir in half & half and cheddar cheese and simmer for another minute. Taste and adjust seasoning if needed.", image: UIImage(named: "Broccoli")!, tags: ["Vegetables", "Dairy"])
    var recipe3 = Article(title: "Zucchini and Fennel Baby Food", paragraph: "Combine olive oil, fennel, and zucchini in a small saucepan over medium heat. Cook and stir until vegetables are softened and have taken some color, about 10 minutes. Pour in water and stir well. Bring to a boil, cover, and reduce heat. Simmer 5 minutes, or until vegetables are soft and fully cooked. Add more water only if mixture is too dry. Remove from heat and let cool slightly. Blend to your desired consistency; for smaller babies, blend until completely smooth.", image: UIImage(named: "Zucchini")!, tags: ["Vegetables"])
    var recipe4 = Article(title: "Ground turkey bolognese", paragraph: "In a large pot of salted boiling water, cook angel hair according to package directions until al dente. Drain and return to pot. Meanwhile, make bolognese: In a large skillet or pot over medium heat, heat oil. Add onion, carrot, celery, and garlic and cook until tender, 5 to 7 minutes. Add ground turkey and cook until no longer pink, 5 minutes more. Season with salt and pepper. Add crushed tomatoes, wine, parsley, and dried oregano. Let simmer until thickened, 10 to 15 minutes. Garnish with Parmesan and parsley.", image: UIImage(named: "Bolognese")!, tags: ["Meat/Fish","Grains"])
    var recipe5 = Article(title: "Spinach and parmesan meatballs", paragraph: "Pour marinara into a 5- or 6- quart slow cooker. Line a rimmed baking sheet with aluminum foil, heat oven to broil with oven rack 4 inches from heat. Toss together Parmesan, panko, parsley, and garlic in a bowl. Season with salt and pepper. Add sausage, beef, and spinach; gently mix to combine. Shape into 20 balls (about 1 1/4 inches); place on prepared baking sheet. Broil until lightly browned, 2 to 3 minutes. Place meatballs in marinara in slow cooker. Cook, covered, until meatballs are cooked through, 3 to 4 hours on high or 5 to 6 hours on low.", image: UIImage(named: "Spinach")!, tags: ["Vegetables","Dairy"])
    
    
    
    
    init(){
        self.recipes = [recipe1, recipe2, recipe3, recipe4, recipe5]
    }
}
