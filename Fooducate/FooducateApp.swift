//
//  FooducateApp.swift
//  Fooducate
//
//  Created by Alessia Saviano on 14/11/21.
//

import SwiftUI

@main
struct FooducateApp: App {
    @AppStorage("isOnboarding") var isOnboarding = true
    @AppStorage("child1Name") var child1Name = "defaultChild"
    @AppStorage("child1Age") var child1Age = 3
    @AppStorage("child1Picked") var child1Picked = false
    @StateObject var childrenStore = ChildrenStore()
    
    var body: some Scene {
        WindowGroup {
            if isOnboarding {
                Welcome().environmentObject(childrenStore)
            } else {
                ContentView().environmentObject(childrenStore)
           }
        }
    }
}

